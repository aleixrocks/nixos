
## Build

```sh
$ nix build .#nixosConfigurations.bsc.config.system.build.toplevel
```

## Install in a running machine

This guide shows how to install nixos in a partition of the current system.

First create a partition using gparted live-usb.

Then, format the partition.

Note: Some grub versions might not recognize an ext4 filesystem with the `metadata_csum_seed` feature, so disable it when formatting.

Also, this names the filesystem as "nixos". Please, ensure that no other filesystem has the same label by inspecting `blkid -o list`.

```sh
# mkfs.ext4 -L nixos -O '^metadata_csum_seed' /dev/<device>
# mount /dev/<device> /mnt
```

Then get your partition UUID. Note that The UUID changes every time you recreate the partition filesystem.

```
ls -l /dev/disk/by-uuid/
```

(`blkid -o list` would also work)

Then edit `m/<machine>/hardware-configuration.nix` and write the UUID for grub.

Then install the system

```sh
$ sudo PATH="$PATH" NIX_PATH="$NIX_PATH" `which nixos-install` --flake .#bsc --root /mnt
```

Then add an entry in your grub to point to the partition containing nixos. Edit `/etc/grub.d/40_custom` and append:

```
menuentry "NixOS" {
    search --set=nixos --label nixos
    configfile "($nixos)/boot/grub/grub.cfg"
}
```

Then rebuild grub

```
# grub2-mkconfig -o /boot/grub/grub.cfg
```

And finally, reboot and select the "NixOS" entry in the grub menu.

## Install in a new machine

Download the nixos live cd and boot into it. If using a dell laptop the boot menu key is F12.

Create, format and mount the parititons accoring the nixos install manual depending of if using grub of UEFI (we assume from now one that this is /mnt, de default nixos-install root).

(optional) Copy the ssh key to access this repo.

Install git and clone this repo

```sh
$ nix install nixpkgs#git
$ nix clone git@gitlab.com:aleixrocks/nixos.git system
```

We need to add the hardware configuration for this new machine. Nixos os generates this for us as follows:

```sh
mkdir ./tmp
nixos-generate-config --root /mnt --dir ./tmp/
```

Copy the hardware config into this repo and check the changes. The initial kernel modules and the UUID for the root and EFI partitions should have changed. 
Also, in the configuration.nix (not the hardware config) select the desired bootloader. If using grub, you MUST specify the disk to install grub. If using EFI, it is not needed. Finally, commit changes and push them. The generated `tmp/configuration.nix` should have already set the bootloader, but we are not copying this file. Instead, we neeed to update it in out current configuration.nix.

```sh
cp /tmp/hardware-configuration.nix system/m/<machine>/hardware-configuration.nix
# VALIDATE CHANGES
# UDPATE bootloader options (if needed)
git add .
git commit -m "Upddated hardware config for new machine"
git push
```

IMPORTANT. The hardware-configuration.nix contains the UUID of the partitions to mount with the `/` and `/efi/`. The parition's UUID changes every time the partition is formatted, so keep sure that you always update the UUID if you end up formatting the partition multiple times. i.e. each time you format the partition, regenerate hardware-configuration.nix

Install nixos

```sh
$ sudo  nixos-install --flake .#bsc --root /mnt
```

Optionally, copy the .ssh and this repo into the new machine

```sh
$ cp -r .ssh system /mnt/home/aleix/
```

Finally reboot and we are done!

## Test without rebooting

Perform a chroot on the nixos partition. Be aware that this does not boot the system! But it is useful to run commands inside without rebooting.

```sh
$ sudo `which nixos-enter` --root /mnt
```

## Bluetooth

If wanting to pair a bluetooth device on nixos that it is already paired on the other linux distro in your same computer, you need to copy the key from your other linux distro to your nixos.
To do so, boot on your other linux distro, mount the nixos partition and copy the key with


```sh
cp -r /var/lib/bluetooth/<MAC OF BLUETOOTH CONTROLLER>/<MAC OF BLUETOOTH DEVICE> /mn/var/lib/bluetooth/<MAC OF BLUETOOTH CONTROLLER>/<MAC OF BLUETOOTH DEVICE>
```

NOTE: there must be an "info" file inside `/var/lib/bluetooth/<MAC OF BLUETOOTH CONTROLLER>/<MAC OF BLUETOOTH DEVICE>/info` on the other linux distro.

## Digital signatures using a smartcard reader

You can digitally sign a document using both firefox and okular pdf viewer.
To check the the smartcard reader is working, use the `pcsc_scan` command line utility.
To check that firefox is working, test it through [FNMT](https://www.sede.fnmt.gob.es/certificados/persona-fisica/verificar-estado), a popup must appear asking for the smartcard certificate password
To sign a document with okular, use the option `tools->Digitally sign`.

## Manual tunnings

 - You might need to install the firefox "Web eID" extension to use a smartcard reader. 

## Init

NOTE: this is not needed if using this repo.

To create an initial nixos configuration under the directory `./tmp` that will be installed in `/mnt`, run:

```sh
mkdir ./tmp
nixos-generate-config --root /mnt --dir ./tmp/
```


# TODO

 - I need to call "configure-gtk" the first time nixos is installed in a machine for the 'Dracula" theme to be applied.
 - I need to manually configure the Dropbox authentication with maestral by running "maestral start" and logging in via web
