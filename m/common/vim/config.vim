"Basic Configuration
syntax on
set autoindent
set number
set hlsearch
filetype plugin indent on
set nowrap

" 80 characters line
set colorcolumn=81
highlight ColorColumn ctermbg=Green ctermfg=DarkRed

"256 colors
set t_Co=256

"see tabs
set list
set listchars=tab:\|\ 

"smpart autocomplete filepaths
set wildmode=longest,list,full
set wildmenu

"show vim-airline
set laststatus=2
let g:airline#extensions#whitespace#enabled = 0

"tagbar shortcut
nmap <F8> :TagbarToggle<CR>

" windwos navigate
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

" theme
colorscheme gruvbox
set background=dark

" per type of file settings
let g:tex_flavor = "latex"
autocmd FileType latex wrap linebreak

" use tabs on python files
augroup python_files
    autocmd!
    autocmd FileType python setlocal noexpandtab
    autocmd FileType python set tabstop=4
    autocmd FileType python set shiftwidth=4
augroup END

" allow backspace to remove in any position
set backspace=indent,eol,start

""""""""""""""""""""""""""""""""""""""""""
"""""""""" Custom Functions """"""""""""""
""""""""""""""""""""""""""""""""""""""""""

" Highlight a column in csv text.
" :Csv 1    " highlight first column
" :Csv 12   " highlight twelfth column
" :Csv 0    " switch off highlight
function! CSVH(colnr)
  if a:colnr > 1
    let n = a:colnr - 1
    execute 'match Keyword /^\([^,]*,\)\{'.n.'}\zs[^,]*/'
    execute 'normal! 0'.n.'f,'
  elseif a:colnr == 1
    match Keyword /^[^,]*/
    normal! 0
  else
    match
  endif
endfunction
command! -nargs=1 Csv :call CSVH(<args>)
