{ config, lib, pkgs, nixpkgs, nixpkgs-unstable, theFlake, ... }:

{
  # Enable flakes
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    util-linux
    wget
    file
    tree
    ncdu
    cscope
    jq
    sshfs
    bc
    python3
    pv
    ldns # for drill
    htop
    btop
    unzip
    ffmpeg
    lsof
    neofetch
    hwloc #lstopo

    # nix tools
    nix-tree
    nixos-option
    nix-diff
    nvd
  ];

  imports = [
    ./bash/default.nix
    ./vim/default.nix
    ./git/default.nix
    ./tmux/default.nix
    ./sysrq.nix
  ];

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "Europe/Madrid";

  environment.variables = {
    EDITOR = "vim";
    VISUAL = "vim";
  };

  # enable serial
  systemd.services."serial-getty@ttyS0" = {
    enable = true;
    wantedBy = [ "getty.target" ];
    serviceConfig.Restart = "always";
  };

  # avoid building if git repo is dirty
  system.configurationRevision =
    if theFlake ? rev
    then theFlake.rev
    else throw "Refusing to build from a dirty Git tree!";

  # set nixpkgs path to the system nixpkgs
  nix.nixPath = [
    "nixpkgs=${nixpkgs}"
  ];
  # set nixpkgs flake to the system nixpkgs
  nix.registry.system.flake = nixpkgs;
  nix.registry.system-unstable.flake = nixpkgs-unstable;
}
