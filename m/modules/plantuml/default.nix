{ pkgs, lib, ... }:

let
  vimOverlay = final: prev: {
    myVimPlugins = (lib.optionals (prev ? myVimPlugins) prev.myVimPlugins) ++ [
      pkgs.vimPlugins.plantuml-syntax
    ];
  };

in {
  environment.systemPackages = with pkgs; [
    plantuml
  ];

  nixpkgs.overlays = [ vimOverlay ];
}
