{ pkgs, pkgs-unstable, lib, ... }:

let
  maptool-macro-vim = pkgs.vimUtils.buildVimPlugin {
    name = "maptool-macro-vim";
    src = pkgs.fetchFromGitHub {
      owner = "cwisniew";
      repo = "maptool-macro-vim";
      rev = "7390773d8f73f5758186eefaa19153bd3bd248d5";
      hash = "sha256-4RWnUtKm0HJs2JX+pQvAR5u/DVZddbN00rrgcqwtKD4=";
    };
  };

  maptoolOverlay = final: prev: {
    myVimPlugins = (lib.optionals (prev ? myVimPlugins) prev.myVimPlugins) ++ [
      maptool-macro-vim
    ];

    openjfx23Maptool = final.openjfx23.override { withWebKit = true; };
    jdk23Maptool = final.jdk23.override { enableJavaFX = true; openjfx23 = final.openjfx23Maptool; };
    maptoolLatest = final.callPackage ./maptool.nix {
      jre = final.temurin-bin-21;
      openjfx = final.openjfx23Maptool;
    };

    #maptoolLatest = final.maptool.overrideAttrs(old: let
    #  version = "1.15.2";
    #  repoBase = "https://github.com/RPTools/maptool";
    #in {
    #  inherit version;
    #  src = final.fetchurl {
    #    url = "${repoBase}/releases/download/${version}/MapTool-${version}.pkg";
    #    hash = "sha256-sKsFVXOF+lm0FCDFXcyTuXtzT3c3PGbZxBSgAurjg9Q=";
    #  };
    #});
  };

in {
  environment.systemPackages = [
    pkgs.maptoolLatest
    #pkgs-unstable.maptool
    pkgs.zip
  ];

  nixpkgs.overlays = [ maptoolOverlay ];
}
