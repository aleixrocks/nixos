{ pkgs, pkgs-unstable, ... }:

{
  imports = [
    #./sway/default.nix
    ./hyprland/default.nix
    ./vscode.nix
    ./spotify.nix
    ./zotero.nix
    ./eid.nix
    ./docker.nix
    ../modules/maptool/default.nix
    ../modules/plantuml/default.nix
  ];

  environment.systemPackages = with pkgs; [
    pavucontrol
    easyeffects
    audacity
    vlc
    imv
    gimp
    inkscape
    yt-dlp
    aria # for yt-dlp fast downloads
    libnotify # for notify-send
    chromium
    evince
    waypipe

    pkgs.logseq
    #pkgs-unstable.zoom-us

    # man pages
    man-pages
    man-pages-posix

    # unfree packages
    discord
    android-studio
  ];

  programs.firefox.enable = true;
  programs.chromium.enable = true;
  programs.thunar.enable = true;
  services.tumbler.enable = true; # Thumbnail support for images

  # enable library and development utilities documentation
  documentation.dev.enable = true;

  # Allow unfree pacakges
  nixpkgs.config.allowUnfree = true;

  # Logseq uses a deprecated electron
  nixpkgs.config.permittedInsecurePackages = [
    "electron-27.3.11"
  ];
}
