{ ... }:

{
  users = {
    users = {
      # Generate hashedPassword with `mkpasswd -m sha-512`
      aleix = {
        isNormalUser = true;
        home = "/home/aleix";
        description = "Aleix Roca Nonell";
        extraGroups = [ "wheel" "networkmanager" "video" ];
        uid = 1000;
        hashedPassword = "$6$SVgImIlJrC0Fb3hR$jIdRfWFV41OqLZytK3voR20ZITeNn9n0QJC3Kh22rQayhEmMQ4sr3k.RdChjTj6uUBplh.QDkNdC35Mt.vYj3/";
      };
    };
  
    #mutableUsers = false;
  };
}
