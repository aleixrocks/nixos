{ pkgs, ... }:

{
  # enable OpenSSH only for localhost connections
  services.openssh = {
    enable = true;
    listenAddresses = [{
      addr = "127.0.0.1";
      port = 22;
    }];
  };
}
