{ pkgs, ... }:

{
  # Setup info
  #   https://nixos.wiki/wiki/Web_eID

  # eID support
  services.pcscd.enable = true;
  programs.firefox.nativeMessagingHosts.packages = with pkgs; [
    web-eid-app
  ];

  # old pkcs#11 support
  # Tell p11-kit to load/proxy opensc-pkcs11.so, providing all available slots
  # (PIN1 for authentication/decryption, PIN2 for signing).
  environment.etc."pkcs11/modules/opensc-pkcs11".text = ''
    module: ${pkgs.opensc}/lib/opensc-pkcs11.so
  '';
  programs.firefox.policies.SecurityDevices.p11-kit-proxy = "${pkgs.p11-kit}/lib/p11-kit-proxy.so";

  # Other applications
  environment.systemPackages = with pkgs; [
    # for signing pdf documents. Go to tools->Digitally sign...
    okular
    # further tools to work with smart cards, recomended by the archlinux wiki, but I think that it is not necessary
    # opensc
    # to test that the smartcards readers works with "pcsc_scan"
    pcsctools
  ];
}
