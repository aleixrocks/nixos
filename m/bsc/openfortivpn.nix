{ nixpkgs, pkgs, ... }:

let
  ofvOverlay = final: prev: {
    openfortivpn = prev.openfortivpn.overrideAttrs (oldAttrs: {
      buildInputs = oldAttrs.buildInputs ++ [
        pkgs.openresolv
      ];
      configureFlags = (pkgs.lib.optional (oldAttrs ? configureFlags) oldAttrs.configureFlags) ++ [
        "--with-resolvconf=${pkgs.openresolv}/bin/resolvconf"
        "--enable-resolvconf"
      ];
    });
  };

in {
  environment.systemPackages = with pkgs; [
    openfortivpn
  ];

  nixpkgs.overlays = [ ofvOverlay ];
}

