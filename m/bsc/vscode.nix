{ pkgs-unstable, vscode-server, vscodeExtensionsFlake, ... }:

let
  systemExtensions = pkgs-unstable.vscode-extensions;
  newestExtensions = vscodeExtensionsFlake.vscode-marketplace;
in {

  # needed to enable remote-ssh. See https://nixos.wiki/wiki/VSCodium
  imports = [
    vscode-server.nixosModules.default
  ];
  # needed to enable remote-ssh. See https://nixos.wiki/wiki/VSCodium
  services.vscode-server.enable = true;

  environment.systemPackages = with pkgs-unstable; [
    (vscode-with-extensions.override {
      vscode = vscodium;
      # to search for more extensions:
      #   https://github.com/nix-community/nix-vscode-extensions?tab=readme-ov-file#explore
      #vscodeExtensions = with vscodeExtensionsFlake.vscode-marketplace; [
      #vscodeExtensions = with vscode-extensions; [
      vscodeExtensions = [
        newestExtensions.ms-vscode-remote.remote-ssh
        systemExtensions.bbenoist.nix
        systemExtensions.ms-python.python
        systemExtensions.ms-vscode.cpptools
        systemExtensions.github.copilot
        systemExtensions.github.copilot-chat
        systemExtensions.eamodio.gitlens
      ];
      #] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
      #  {
      #    name = "remote-ssh-edit";
      #    publisher = "ms-vscode-remote";
      #    version = "0.47.2";
      #    sha256 = "1hp6gjh4xp2m1xlm1jsdzxw9d8frkiidhph6nvl24d0h8z34w49g";
      #  }
      #];
    })
  ];

  environment.sessionVariables.NIXOS_OZONE_WL = "1";
}
