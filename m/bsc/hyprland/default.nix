{ config, hyprland, pkgs, pkgs-unstable, ... }:

let
  #hyprlandPackage = pkgs-unstable.hyprland;
  #portalPackage = pkgs-unstable.xdg-desktop-portal-hyprland;
  hyprlandPackage = hyprland.packages.${pkgs.stdenv.hostPlatform.system}.hyprland;
  portalPackage   = hyprland.packages.${pkgs.stdenv.hostPlatform.system}.xdg-desktop-portal-hyprland;
  mesaPackage     = hyprland.inputs.nixpkgs.legacyPackages.${pkgs.stdenv.hostPlatform.system}.mesa.drivers;

  hyprland-config = pkgs.writeShellScriptBin "Hyprland-config" ''
    ${hyprlandPackage}/bin/Hyprland -c /etc/hyprland/config &> /tmp/hyprland.log
  '';

  # currently, there is some friction between wayland and gtk:
  # https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland
  # the suggested way to set gtk settings is with gsettings
  # for gsettings to work, we need to tell it where the schemas are
  # using the XDG_DATA_DIR environment variable
  # run at the end of sway config
  configure-gtk = pkgs.writeTextFile {
    name = "configure-gtk";
    destination = "/bin/configure-gtk";
    executable = true;
    text = let
      schema = pkgs.gsettings-desktop-schemas;
      datadir = "${schema}/share/gsettings-schemas/${schema.name}";
    in ''
      export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
      gnome_schema=org.gnome.desktop.interface
      gsettings set $gnome_schema gtk-theme 'Dracula'
    '';
  };
  wallpapers = pkgs.callPackage ./wallpapers.nix {};
  waybarLoop = pkgs.callPackage ./waybar_loop.nix {};

in {
  programs.hyprland = {
    enable = true;
    package = hyprlandPackage;
    portalPackage = portalPackage;

    #portalPackage = pkgs-unstable.xdg-desktop-portal-hyprland.override {
    #  inherit (pkgs-unstable) mesa;
    #};
  };
  # needed to ensure that the system and hyprland use the same mesa version
  # see https://github.com/hyprwm/Hyprland/issues/5148
  hardware.graphics = {
    # Apparently, this is now causing issues: https://github.com/hyprwm/Hyprland/issues/9062
    # anyways it should be imported from the hpyprland flake, not the pkgs-unstable.
    package = mesaPackage;
    enable = true;
    # if you also want 32-bit support (e.g for Steam)
    # driSupport32Bit = true;
    # package32 = pkgs-unstable.pkgsi686Linux.mesa.drivers;
  };

  # Optional, hint electron apps to use wayland:
  environment.sessionVariables.NIXOS_OZONE_WL = "1";

  environment.systemPackages = with pkgs; [
    hyprland-config
    kitty
    pkgs-unstable.alacritty
    mako
    wofi
    playerctl
    waypipe
    xdg-utils # for opening default programs when clicking links
    waybarLoop

    # clipboard
    wl-clipboard
    cliphist

    # screenshot
    grim
    slurp

    # monitors
    kanshi

    # lockscreen
    pkgs-unstable.hypridle
    pkgs-unstable.hyprlock
    pkgs-unstable.hyprsunset
    pkgs-unstable.hyprpaper

    # gtk theme
    glib # gsettings
    configure-gtk
    dracula-theme
    adwaita-icon-theme  # default gnome cursors

    # wallpapers
    wallpapers
  ];

  # for waybar icons
  fonts.packages = with pkgs; [
    font-awesome
  ];

  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
    wireplumber.enable = true;
  };

  # enable brightness control program
  programs.light.enable = true;
  
  # it does not autostart
  programs.nm-applet.enable = true; # GUI for network manager

  # Monitor configuration daemon
  systemd.user.services.kanshi = {
    description = "kanshi daemon";
    serviceConfig = {
      Type = "simple";
      ExecStart = ''${pkgs.kanshi}/bin/kanshi -c /etc/kanshi.conf'';
      Restart = "on-failure";
      RestartSec = 5;
    };
  };

  # enable hyprland cache
  nix.settings = {
    substituters = ["https://hyprland.cachix.org"];
    trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
  };

  # Allow password authentication in swaylock
  security.pam.services.swaylock = {};

  # Add configuration files
  environment.etc."hyprland/config".source = ./hyprland.conf;
  environment.etc."hyprland/waybar/config.jsonc".source = ./waybar/config.jsonc;
  environment.etc."hyprland/waybar/style.css".source = ./waybar/style.css;
  environment.etc."hyprland/hypridle.conf".source = ./hypridle.conf;
  environment.etc."hyprland/hyprlock.conf".source = ./hyprlock.conf;
  environment.etc."hyprland/hyprpaper.conf".source = ./hyprpaper.conf;
  environment.etc."kanshi.conf".source = ./kanshi/config;
}
