{writeShellScriptBin}:

writeShellScriptBin "wallpapers" ''
  dir=~/wallpapers
  monitor=""
  
  if [ -d "$dir" ]; then
      random_background=$(ls $dir/* | shuf -n 1)
  
      hyprctl hyprpaper unload all
      hyprctl hyprpaper preload $random_background
      hyprctl hyprpaper wallpaper "$monitor, $random_background"
  fi
''
