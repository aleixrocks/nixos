{writeShellScriptBin, waybar}:

writeShellScriptBin "waybar-loop" ''
  if [[ $(pgrep --exact waybar) ]]; then
    exit 0
  fi
  while true; do
    ${waybar}/bin/waybar -c /etc/hyprland/waybar/config.jsonc -s /etc/hyprland/waybar/style.css
    sleep 2
  done
''

