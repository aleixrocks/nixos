{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    zotero
    poppler_utils
  ];
}
