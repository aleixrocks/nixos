{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    maestral
  ];

  systemd.services.maestral = {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    description = "Start the maestral client";
    serviceConfig = {
      Type = "forking";
      User = "aleix";
      ExecStart = ''${pkgs.maestral}/bin/maestral start'';
      ExecStop  = ''${pkgs.maestral}/bin/maestral stop'';
    };
  };
}
