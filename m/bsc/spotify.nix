{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # unfree packages
    spotify
  ];

  # (arch) Detect other devices on the network
  networking.firewall.allowedTCPPorts = [ 57621 ];

  networking.firewall.allowedUDPPorts = [
    # (arch) detect other devices on the local network
    # (nixos) sync local tracks from your filesystem with mobile devices in the same network
    57621
    # Enable discovery of Google Cast devices (and possibly other Spotify Connect
    # devices) in the same network by the Spotify app
    5353
  ];
}
