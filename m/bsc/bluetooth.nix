{ ... }:

{
  # enable support for Bluetooth
  hardware.bluetooth.enable = true; 
  # Power up the default Bluetooth controller
  hardware.bluetooth.powerOnBoot = true;
  # Add blueman service with blueman-applet and blueman-manager
  services.blueman.enable = true;
}
