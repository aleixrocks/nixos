{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    #hyprland.url = "github:hyprwm/Hyprland?ref=v0.47.2";
    hyprland.url = "github:hyprwm/Hyprland";

    # vscode
    nix-vscode-extensions.url = "github:nix-community/nix-vscode-extensions";
    nix-vscode-extensions.inputs.nixpkgs.follows = "nixpkgs";
    vscode-server.url = "github:nix-community/nixos-vscode-server";
    vscode-server.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, nixpkgs-unstable, hyprland, nix-vscode-extensions, vscode-server }:
let
  mkConf = name: nixpkgs.lib.nixosSystem rec {
    system = "x86_64-linux";
    specialArgs = {
      inherit nixpkgs nixpkgs-unstable;
      inherit hyprland;
      vscodeExtensionsFlake = nix-vscode-extensions.extensions.${system};
      inherit vscode-server;
      pkgs-unstable = import nixpkgs-unstable {
        inherit system;
        config.allowUnfree = true;
      };
      theFlake = self;
    };
    modules = [ "${self.outPath}/m/${name}/configuration.nix" ];
  };
in
  {
    nixosConfigurations = {
      bsc   = mkConf "bsc";
      rocks = mkConf "rocks";
    };
  };
}
